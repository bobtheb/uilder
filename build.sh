#!/bin/sh

apk add bash git
git clone https://github.com/untoreh/scripts -b utils utils
cd utils/buildscripts
./xmrigCCMin-static-build.sh || exit 1

./xmrigCCServer-dynamic-build.sh || exit 1

